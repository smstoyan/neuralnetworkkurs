#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include <serveraccess.h>
#include <statecache.h>

namespace Ui {
class MW;
}

class MW : public QMainWindow
{
    Q_OBJECT

public:
    explicit MW(QWidget *parent = 0);
    ~MW();

private:
    Ui::MW *ui;
    ServerAccess *srv;
    StateCache *cache;
};

#endif // MW_H
