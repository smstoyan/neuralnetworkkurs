#include "serveraccess.h"
#include "QJsonDocument"
#include "QJsonObject"
#include "QJsonValue"
#include "QJsonArray"
#include "QJsonParseError"
#include "QDebug"
#include "assert.h"

ServerAccess::ServerAccess(QObject *parent) :
    QObject(parent)
{
    NetAM = new QNetworkAccessManager(this);
    connect(NetAM,SIGNAL(finished(QNetworkReply*)),this,SLOT(proceedAnswer(QNetworkReply*)));
    connect (&timer,SIGNAL(timeout()), this, SLOT(getFieldState()));
    cache = 0;
    serverCreated = false;
}

void ServerAccess::initField(quint16 size1, quint16 size2)
{
    if (cache != NULL)
    {
        cache->allocate(size1,size2);
        QUrl url = QUrl(initString.arg(size1).arg(size2));
        NetAM->get(QNetworkRequest(url));
    }
}

void ServerAccess::proceedAnswer(QNetworkReply *reply)
{
    if (!serverCreated)
    {
        serverCreated = true;
        timer.start(updateInterval);
        return;
    }
    if (reply->error() == QNetworkReply::NoError)
    {
        //        qDebug()<<QString(reply->readAll());
        QJsonParseError err;
        QJsonDocument jsonReply = QJsonDocument::fromJson(reply->readAll(),&err);
        if (err.error == QJsonParseError::NoError)
        {
            QJsonArray arr = jsonReply.array();
            cache->setData(arr);
        }
    }
    else
    {
        qDebug()<<"error!";
    }
}

void ServerAccess::getFieldState()
{
    NetAM->get(QNetworkRequest(QUrl(getFieldString)));
}

void ServerAccess::setCache(StateCache *cache)
{
    this->cache = cache;
}
