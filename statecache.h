#ifndef STATECACHE_H
#define STATECACHE_H

#include <QObject>
#include <QJsonArray>
#include <QRect>

class StateCache : public QObject
{
    Q_OBJECT
public:
    explicit StateCache(QObject *parent = 0);
    ~StateCache();
    void setData(QJsonArray arr);
    void allocate(quint16 width, quint16 height);
    int getValue(int x, int y);
    QRect getSize();
signals:
    void update();
    void rescale();
public slots:
private:
    int *data;
    int *pointer;
    int allocated;
    quint16 dataWidth;
    quint16 dataHeight;
};

#endif // STATECACHE_H
