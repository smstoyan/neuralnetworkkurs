#ifndef NEUROSCENE_H
#define NEUROSCENE_H

#include <QWidget>
#include <statecache.h>
#include <QPixmap>

class NeuroScene : public QWidget
{
    Q_OBJECT
public:
    explicit NeuroScene(QWidget *parent = 0);

    void setScale(float scale);
    void setSceneSize(int width, int height);
    void setCache(StateCache *cache);

    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *ev);
    void mouseMoveEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void mouseDoubleClickEvent(QMouseEvent *);
    void wheelEvent(QWheelEvent *ev);


    static const float defaultScale;
    static const int defaultBlockSize;
    static const float maxScale;
    static const float minScale;
signals:

public slots:
    void changeSceneSize();
private:
    QRect sceneSize;
    int currX, currY;
    int lastX, lastY;
    int lastCX, lastCY;
    float currScale;
    int blocksCountX, blocksCountY;
    int blockSize;
    void drawPix(int code, int x, int y, QPainter *p);

    QPixmap buffer;
    StateCache *cache;
    QPixmap grass;
    QPixmap animal1;
    QPixmap animal2;
};

#endif // NEUROSCENE_H



















