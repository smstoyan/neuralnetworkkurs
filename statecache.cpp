#include "statecache.h"
#include "QVariant"

StateCache::StateCache(QObject *parent) :
    QObject(parent)
{
    data = NULL;
    allocated = 0;
}

StateCache::~StateCache()
{
    delete data;
}

void StateCache::setData(QJsonArray arr)
{
    int *ptr = data;
    for (int i = 0; i < dataHeight; i ++)
    {
        QJsonArray tmp = arr.at(i).toArray();
        for (int j = 0; j < dataWidth; j++)
        {
            (*ptr) = tmp.at(j).toVariant().toInt();
            ptr++;
        }
    }
    emit update();
}

void StateCache::allocate(quint16 width, quint16 height)
{
    if (data)
        delete data;
    quint16 dataSize = width * height;
    data = new int[dataSize];
    for(int i = 0; i < dataSize; i++)
        data[i] = 0;
    dataWidth = width;
    dataHeight = height;
    emit rescale();
}

int StateCache::getValue(int x, int y)
{
//    if ((x < dataWidth) && (y < dataHeight))
//    {
        int result = data[y*dataWidth + x];
        return result;
//    }
//    else
//        return 0;
}

QRect StateCache::getSize()
{
    return QRect(0,0,dataWidth,dataHeight);
}
