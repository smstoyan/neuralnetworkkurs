#-------------------------------------------------
#
# Project created by QtCreator 2013-12-01T21:50:03
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NeuralNetworkGUI
TEMPLATE = app


SOURCES += main.cpp\
        mw.cpp \
    neuroscene.cpp \
    serveraccess.cpp \
    statecache.cpp

HEADERS  += mw.h \
    neuroscene.h \
    serveraccess.h \
    statecache.h

FORMS    += mw.ui

RESOURCES += \
    resources.qrc
