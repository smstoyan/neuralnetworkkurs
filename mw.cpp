#include "mw.h"
#include "ui_mw.h"


MW::MW(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MW)
{
    ui->setupUi(this);
    ui->centralWidget->setLayout(ui->gridLayout);
    srv = new ServerAccess(this);
    cache = new StateCache(this);
    ui->neuroScene->setCache(cache);
    srv->setCache(cache);
    srv->initField(50,50);
}

MW::~MW()
{
    delete ui;
}
