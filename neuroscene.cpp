#include "neuroscene.h"
#include <QPainter>
#include <QWheelEvent>
#include <QDebug>

const float NeuroScene::defaultScale = 1;
const int NeuroScene::defaultBlockSize = 10;
const float NeuroScene::maxScale = 5;
const float NeuroScene::minScale = 0.3;

NeuroScene::NeuroScene(QWidget *parent) :
    QWidget(parent)
{
    currX = currY = 0;
    currScale = defaultScale;
    blockSize = defaultBlockSize;
    cache = NULL;
    setSceneSize(0,0);
    grass.load(":/images/grass.png");
    animal1.load(":/images/cat_grumpy.png");
    animal2.load(":/images/cat_fight.png");
}

void NeuroScene::paintEvent(QPaintEvent *)
{
    if (cache != NULL)
    {
        QPainter p;
        p.begin(this);
        p.fillRect(this->rect(),Qt::white);
        int first, last, bmin, bmax;

        first = currX < 0 ? currX % blockSize : currX;
        last = -currX + width() < blocksCountX * blockSize ? first + width() : blocksCountX * blockSize + currX;
        bmin = currY < 0 ? currY % blockSize : currY;
        bmax = -currY + height() < blocksCountY * blockSize ? height() : currY + blocksCountY * blockSize;

        for (int i = first; i <= last; i+=blockSize)
        {
            p.drawLine(i,bmin,i,bmax);
        }

        first = currY < 0 ? currY % blockSize : currY;
        last = -currY + height() < blocksCountY * blockSize ? first + height(): blocksCountY * blockSize + currY;
        bmin = currX < 0 ? currX % blockSize : currX;
        bmax = -currX + width() < blocksCountX * blockSize ? width() : currX + blocksCountX * blockSize;

        for (int i = first; i <= last;i+=blockSize)
        {
            p.drawLine(bmin,i,bmax,i);
        }

        first = currX < 0 ? - currX / blockSize : 0;
        last = width() - currX < blocksCountX * blockSize ? (width() - currX) / blockSize + 1: blocksCountX;
        bmin = currY < 0 ? - currY / blockSize : 0;
        bmax = height() - currY < blocksCountY * blockSize ? (height() - currY) / blockSize + 1: blocksCountY;
        for (int i = bmin; i < bmax; i++)
        {
            for (int j = first; j < last; j++)
            {
                drawPix(cache->getValue(j,i),j,i,&p);
            }
        }
        p.end();
    }
}

void NeuroScene::drawPix(int code, int x, int y, QPainter *p)
{
    QPixmap res;
    switch (code) {
    case 0:
        break;
    case -1:
        res = grass.scaled(blockSize,blockSize);
        break;
    default:
        if (code % 2)
            res = animal1.scaled(blockSize,blockSize);
        else
            res = animal2.scaled(blockSize,blockSize);
        break;
    }
    if (code)
    {
        int xpos, ypos;
        xpos = x * blockSize + currX;
        ypos = y * blockSize + currY;
        p->drawPixmap(xpos,ypos,blockSize, blockSize,res);
    }
}

void NeuroScene::mousePressEvent(QMouseEvent *ev)
{
    switch (ev->buttons())
    {
    case Qt::LeftButton:
        this->setMouseTracking(true);
        lastCX = currX;
        lastCY = currY;
        lastX = ev->x();
        lastY = ev->y();
        break;
    case Qt::MiddleButton:
        setScale(defaultScale);
        break;
    default:
        break;
    }

}

void NeuroScene::mouseMoveEvent(QMouseEvent *ev)
{
    if(ev->buttons() == Qt::LeftButton)
    {
        currX = lastCX - (lastX - ev->x());
        currY = lastCY - (lastY - ev->y());
        update();
    }
}

void NeuroScene::mouseReleaseEvent(QMouseEvent *ev)
{
    if (ev->buttons() == Qt::LeftButton)
    {
        this->setMouseTracking(false);
    }
}

void NeuroScene::mouseDoubleClickEvent(QMouseEvent *)
{

}

void NeuroScene::wheelEvent(QWheelEvent *ev)
{
    if (ev->delta() > 0)
    {
        setScale(currScale * 1.1);
    }
    else if (ev->delta() < 0)
    {
        setScale(currScale / 1.1);
    }
}

void NeuroScene::setScale(float scale)
{
    if (scale < minScale)
        scale = minScale;
    else if (scale > maxScale)
        scale = maxScale;
    currScale = scale;
    blockSize = defaultBlockSize * currScale;
    update();
}

void NeuroScene::setSceneSize(int width, int height)
{
    blocksCountX = width;
    blocksCountY = height;
    sceneSize = QRect(0,0,width, height);
}

void NeuroScene::setCache(StateCache *cache)
{
    if (this->cache)
    {
        //add disconnecting signal/slots from previous cache
    }
    this->cache = cache;
    changeSceneSize();
    connect(cache,SIGNAL(rescale()),this,SLOT(changeSceneSize()));
    connect(cache,SIGNAL(update()),this,SLOT(update()));
}

void NeuroScene::changeSceneSize()
{
    QRect tmp = cache->getSize();
    setSceneSize(tmp.width(),tmp.height());
}
