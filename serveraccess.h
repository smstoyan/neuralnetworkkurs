#ifndef SERVERACCESS_H
#define SERVERACCESS_H

#include <QObject>
#include <QUrl>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QTimer>
#include <statecache.h>

class ServerAccess : public QObject
{
    Q_OBJECT
public:
    explicit ServerAccess(QObject *parent = 0);
    void setCache(StateCache *cache);
    void initField(quint16 size1 = 50, quint16 size2 = 50);
signals:

public slots:
    void getFieldState();
private slots:
    void proceedAnswer(QNetworkReply*);
private:
    QNetworkAccessManager *NetAM;
    const QString initString = "http://localhost:3000/init/%1/%2";
    const QString registerString = "http://localhost:3000/register/1";
    const QString getStateString = "http://localhost:3000/get_player_state";
    const QString getFieldString = "http://localhost:3000/get_field_state";
    const QString turnString = "http://localhost:3000/turn/1/left";
    const int updateInterval = 500;
    StateCache *cache;

    bool serverCreated;
    QTimer timer;
};

#endif // SERVERACCESS_H
